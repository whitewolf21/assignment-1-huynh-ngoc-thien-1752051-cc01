package net.codejava;

public class Knight extends Fighter {
	
	public Knight(int baseHp, int wp) {
		super(baseHp, wp);
	}
	
	public double getCombatScore() {
		if(Utility.Ground == 999)
		{
			double a = 0.0;
			double b = 1.0;
			double temp = 0.0;
			while(a <= this.getBaseHp() * 2)
			{
				temp = a;
				a = b;
				b = temp + b;
			}
			return a;
		}
		else if(Utility.isSquare(Utility.Ground))
			return this.getBaseHp() * 2;
		else
		{
			if (this.getWp() == 1)
				return this.getBaseHp();
			else
				return this.getBaseHp() / 10;
		}
	}
}