package net.codejava;

public class Fighter {
	
	private int mBaseHp;
	private int mWp;
	
	public double getCombatScore() {
		return 0.0;
	}
	
	public Fighter(int BaseHp, int Wp) {
		this.mBaseHp = BaseHp;
		this.mWp = Wp;
	}
	
	public double getBaseHp() {
		return mBaseHp;
	}
	
	public void setBaseHp(int baseHp) {
		this.mBaseHp = baseHp;
	}
	
	public int getWp() {
		return mWp;
	}
	
	public void setWp(int wp) {
		this.mWp = wp;
	}
}